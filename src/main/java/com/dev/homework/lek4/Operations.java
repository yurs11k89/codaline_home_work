package com.dev.homework.lek4;

import java.util.Set;

/**
 * Created by yurik on 27.02.16.
 */
public interface Operations {

    boolean equals(Set a, Set b);

    Set union(Set a, Set b);

    Set subtract(Set a, Set b);

    Set intersect(Set a, Set b);

    Set symetrikSubtract(Set a, Set b);
}
