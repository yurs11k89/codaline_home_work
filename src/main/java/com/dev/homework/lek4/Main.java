package com.dev.homework.lek4;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by yurik on 27.02.16.
 */
public class Main{

    public static void main(String[] args) {
        Operations operations = new Operations() {
            @Override
            public boolean equals(Set a, Set b) {
                if (a.equals(b)){
                    return true;
                }else {
                    return false;
                }
            }

            @Override
            public Set union(Set a, Set b) {
                a.addAll(b);
                return a;
            }

            @Override
            public Set subtract(Set a, Set b) {
                Set<Integer> d = new TreeSet<Integer>(a);
                d.removeAll(b);
                return d;
            }

            @Override
            public Set intersect(Set a, Set b) {
                Set<Integer> d = new TreeSet<Integer>(a);
                d.retainAll(b);
                return d;
            }

            @Override
            public Set symetrikSubtract(Set a, Set b) {
                Set<Set<Integer>> ab = new TreeSet<>(a);
                ab.removeAll(b);
                Set<Integer> ba = new TreeSet<>(b);
                ba.removeAll(a);
                ab.add(ba);
                return ab;
            }
        };

        Set<Integer> a = new HashSet<>();
        Set<Integer> b = new HashSet<>();
        a.add(1);
        a.add(2);
        a.add(3);

        b.add(1);
        b.add(2);

        //System.out.println(operations.equals(a, b)); // equals
        //System.out.println(operations.union(a, b));  // union
        //System.out.println(operations.subtract(a, b));  //subtract
        //System.out.println(operations.intersect(a, b));  //intersect
        //System.out.println(operations.symetrikSubtract(a, b));  //symetrikSubtract
    }

}
