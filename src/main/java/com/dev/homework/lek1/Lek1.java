package com.dev.homework.lek1;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by ���� on 04.02.2016.
 */
public class Lek1 {

    private static int[] arr = {1, 2, 3, 5, 10, 8, 9, 17, 20};

    public static void main(String[] args) {
//-----------------------------
//        System.out.println("Середнє арифметичне = " + sum(arr));
//-----------------------------

//        System.out.println("Введіть значення ...");
//        Scanner scanner = new Scanner(System.in);
//        int f = scanner.nextInt();
//        System.out.println("Факторіал '" + f + "' = " + faktorial(f));

//-----------------------------
//        searchPrice();
//-----------------------------

        searchInt();
    }

    private static void searchInt() {
        System.out.println("Введіть значення змінної! ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();


        if (n == 0) {
            System.out.println("Число ні парне, ні не парне");

        } else if((n%2)==0){
            System.out.println("Число " + n + " парне ");
        }
        else{
            if((n%1)==0){
                System.out.println("Число " + n + " непарне ");
            }
        }

    }

    private static void searchPrice() {
        System.out.println("Введіть код товару ");
        Scanner scanner = new Scanner(System.in);
        int code = scanner.nextInt();
        String price = getPrice(code);

        if (code == 0) {
            System.out.println("Exit");
            return;
        } else if (price != null) {
            System.out.println("Ціна товару = " + price);
            searchPrice();
        } else {
            System.out.println("Товар не знайдено");
            searchPrice();
        }
    }

    private static String getPrice(int code) {
        Map<Integer, String> base = new HashMap<>();
        base.put(123, "12:30");
        base.put(1210, "5:40");
        base.put(1212, "3:20");
        base.put(1234, "1:50");
        base.put(121, "100");


        return  base.get(code);
    }

    private static int faktorial(int n) {
        int result;

        if (n == 1)
            return 1;
        result = faktorial(n - 1) * n;

        return result;
    }

    public static int sum(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }

        return sum / array.length;
    }
}
