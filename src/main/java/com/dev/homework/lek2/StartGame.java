package com.dev.homework.lek2;

import com.dev.homework.lek2.cat.Cat;
import com.dev.homework.lek2.dog.Dog;

import java.util.Scanner;

/**
 * Created by Юрик on 09.02.2016.
 */
public class StartGame {

    private static Pet pet;
    private static int petNumber;

    public static void main(String[] args) {
        System.out.println("Оберіть тваринку : 1 - Кіт , 2 - Пес");
        Scanner scanner = new Scanner(System.in);
        boolean b = true;
        while (b){
            petNumber = scanner.nextInt();
            if(petNumber > 2){
                System.out.println("Неправильно обране число((( Оберіть між числами 1 або 2!!!");
            }else {
                b = false;
            }
        }

        if(petNumber == 1){
            System.out.println("Ви обрали Кота!");
            pet = new Cat();

        }else {
            System.out.println("Ви обрали Пса!");
            pet = new Dog();
        }

            pet.startInf();
            while (true){
                System.out.println("1 - говорити, 2 - їсти, 3 - спати , 4 - грати");
                int met = new Scanner(System.in).nextInt();
                if(met == 1 || met == 2 || met == 3 || met == 4){
                    pet.startMethod(met);
                }else {
                    System.out.println("Неправильно обрана дія ! Такої дії не існує !");
                }
                if(pet.getFatigue() <= 0){
                    System.out.println("Твою тваринку відвезли в лікарню ((( Гра закінчена((((");
                    System.exit(0);
                }
            }



    }
}
