package com.dev.homework.lek2.dog;

import com.dev.homework.lek2.Pet;

/**
 * Created by Юрик on 10.02.2016.
 */
public class Dog implements Pet {

    private int eatValue = 100;     //
    private int sleepValue = 80;    // не хотіння спати
    public int fatigue = 87;     // рівень бодрості

    private void sayGav() {
        System.out.println("Гав - гав");
        sleepValue = sleepValue - 20;
        eatValue = eatValue - 10;
        fatigue = fatigue - 20;
    }

    private void eat() {
        if (eatValue <= 81) {
            System.out.println("їжа з`їдена");
            eatValue = eatValue + 19;
            if (fatigue <= 85) {
                fatigue = fatigue + 15;
            }
            sleepValue = sleepValue - 15;
        } else {
            System.out.println("Не хочу їсти!!!");
        }
    }

    private void sleep() {
        if (sleepValue <= 80) {
            System.out.println("Ліг спати");
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("Прокинувся");
            sleepValue = sleepValue + 30;
            eatValue = eatValue - 20;
            if (fatigue <= 90) {
                fatigue = fatigue + 10;
            }
        } else {
            System.out.println("Не хочу спати");
        }

    }

    private void playGame() {
        System.out.println("Гра завершена");
        fatigue = fatigue - 40;
        eatValue = eatValue - 30;
        sleepValue = sleepValue - 30;
    }

    @Override
    public int getFatigue() {
        return fatigue;
    }

    @Override
    public void startMethod(int m) {
        switch (m) {
            case 1:
                sayGav();
                break;
            case 2:
                eat();
                break;
            case 3:
                sleep();
                break;
            case 4:
                playGame();
                break;
            case 5:
                System.exit(0);
                break;
        }
        startInf();
    }

    @Override
    public void startInf() {
        System.out.println("статус сну = " + sleepValue + "%;" + " ситність = " + eatValue + "%;" + " статус бодрості " + fatigue + "%");
    }
}
