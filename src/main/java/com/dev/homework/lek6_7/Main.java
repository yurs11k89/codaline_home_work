package com.dev.homework.lek6_7;

import com.google.gson.Gson;

/**
 * Created by yurik on 19.03.16.
 */
public class Main {

    static String json;

    public static void main(String[] args) {

        Student obj = new Student();
        Gson gson = new Gson();

        // convert java object to JSON format,
        // and returned as JSON formatted string
        new Thread(() -> {
            json = gson.toJson(obj);
            System.out.println(json);
        }).start();
    }
}
