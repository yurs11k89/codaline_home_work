package com.dev.homework.lek6_7;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yurik on 19.03.16.
 */
public class Student {
    private int age = 20;
    private String name = "Yurik";
    private List<String> skills = new ArrayList<String>() {
        {
            add("realm");
            add("retrofit");
            add("palette");
        }
    };

    //getter and setter methods

    @Override
    public String toString() {
        return "Student [age=" + age + ", name=" + name + ", skills="
                + skills + "]";
    }
}
