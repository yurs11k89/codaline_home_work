package com.dev.homework.lek8;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * Created by yurik on 04.04.16.
 */
public class Maim {

    public static final String URL = "jdbc:mysql://localhost:3306/mydbtest";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "root";


    public static void main(String[] args) {
        Connection connection;


        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
        }catch (Exception e){
            System.out.println("Не вдалось звантажити драйвер");
        }

        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            Statement statement = connection.createStatement();
            statement.execute("INSERT INTO users (name, surename) VALUE ('Kostya', 'Klugman')");


            connection.close();
        }catch (Exception e){

        }
    }
}
